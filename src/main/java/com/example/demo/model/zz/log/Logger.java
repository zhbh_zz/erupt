package com.example.demo.model.zz.log;

/*
 *
 * Author: zouzhao
 */

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
import xyz.erupt.annotation.*;
import xyz.erupt.annotation.sub_field.*;
import xyz.erupt.annotation.sub_field.sub_edit.*;
import xyz.erupt.jpa.model.BaseModel;

@Erupt(name = "仓储日志")
@Table(name = "WH_LOG")
@Entity
@Getter
@Setter
public class Logger extends BaseModel {

    @EruptField(
            views = @View(
                    title = "IP"
            ),
            edit = @Edit(
                    title = "IP",
                    type = EditType.AUTO, search = @Search, notNull = true
            )
    )
    private String ip;

    @EruptField(
            views = @View(
                    title = "目标类"
            ),
            edit = @Edit(
                    title = "目标类",
                    type = EditType.AUTO, search = @Search, notNull = true
            )
    )
    private String className;

    @EruptField(
            views = @View(
                    title = "日志类型"
            ),
            edit = @Edit(
                    title = "日志类型",
                    type = EditType.AUTO, search = @Search, notNull = true,
                    inputType = @InputType(length = 10)
            )
    )
    private String level;

    @EruptField(
            views = @View(
                    title = "方法"
            ),
            edit = @Edit(
                    title = "方法",
                    type = EditType.AUTO, search = @Search, notNull = true
            )
    )
    private String methodName;

    @EruptField(
            views = @View(
                    title = "错误代码行"
            ),
            edit = @Edit(
                    title = "错误代码行",
                    type = EditType.AUTO, search = @Search, notNull = true
            )
    )
    private Long lineNumber;

    @EruptField(
            views = @View(
                    title = "文件名称"
            ),
            edit = @Edit(
                    title = "文件名称",
                    type = EditType.AUTO, search = @Search, notNull = true
            )
    )
    private String fileName;

    @EruptField(
            views = @View(
                    title = "错误信息"
            ),
            edit = @Edit(
                    title = "错误信息",
                    type = EditType.AUTO, search = @Search, notNull = true
            )
    )
    private String msg;

    @Lob
    @EruptField(
            views = @View(title = "异常详细信息", type = ViewType.HTML),
            edit = @Edit(title = "异常详细信息", type = EditType.HTML_EDITOR,
                    htmlEditorType = @HtmlEditorType(HtmlEditorType.Type.CKEDITOR))
    )
    private String errorDetail;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Long getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErrorDetail() {
        return errorDetail;
    }

    public void setErrorDetail(String errorDetail) {
        this.errorDetail = errorDetail;
    }
}
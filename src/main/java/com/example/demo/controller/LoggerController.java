package com.example.demo.controller;

import com.example.demo.model.zz.log.Logger;
import com.example.demo.model.zz.log.LoggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;


@RestController
@RequestMapping("/log")
public class LoggerController {

    @Autowired
    private LoggerRepository loggerRepository;

    @RequestMapping(value ="/create" , method = RequestMethod.POST)
    @ResponseBody
    public Logger createLog(@RequestBody Logger logger , HttpServletRequest request) throws InvocationTargetException, IllegalAccessException {
        return loggerRepository.save(logger);
    }



}